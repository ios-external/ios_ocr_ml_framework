//
//  NativeLib.h
//  ios_ocr_ml
//
//  Created by sanjay.k@karza.in on 19/04/21.
//

#import <Foundation/Foundation.h>

@interface NativeLib : NSObject
+(long)start;
+(void)initAnalyzer:(NSData*)docData blurData:(NSData*)blurData;
+(NSArray *)runInf:(NSArray *)input bitmapWidth:(int)bitmapWidth bitmapHeight:(int)bitmapHeight inputImageWidth:(int)inputImageWidth inputImageHeight:(int)inputImageHeight;
+(void)releaseKAnalyzer;
+(void)temp;
+(NSString *)getCacheKey;
+(NSString *)getExpPKey;
+(NSString *)getExpPrKey;
+(float)blurScore:(NSArray *)imageData imageWidth:(int)imageWidth imageHeight:(int)imageHeight;
+(int)brightnessCheck:(float)r g:(float)g b:(float)b;
@end
